package grpwk;

import java.util.ArrayList;
import java.util.SortedMap;

public class Argparse
{
    static enum Type {
        Str,
        Int,
        Double
    };
    ArrayList<String> args = new ArrayList<>();
    SortedMap<String,String> pholder2Arg;
    SortedMap<String, Type>  typeMap;
    String description;

    Argparse(String description,String[] _args)
    {
        this.description = description;
        for(var arg : _args)
            args.add(arg);
    }

    void add_argument(String placeholder,Type retType)
    {
        pholder2Arg.put(placeholder,"");
        typeMap.put(placeholder,retType);
    }

    public void parse_args()
    {

    }

    public class GetValue<T>
    {
        public T value(SortedMap<String,Type> typeMap,String placeholder)
        {
            T value;
            switch(typeMap.get(placeholder))
            {
                case Str:
                    value = (T)pholder2Arg.get(placeholder);
                    break;
                case Int:
                    value = (T)pholder2Arg.get(placeholder);
                    break;
                case Double:
                    value = (T)pholder2Arg.get(placeholder);
                    break;
                default:
                    value = (T)pholder2Arg.get(placeholder);
                    break;
            }

            return value;
        }
    }
}