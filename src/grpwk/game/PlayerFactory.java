package grpwk.game;

import java.security.InvalidParameterException;
import java.util.ArrayList;

public abstract class PlayerFactory {
    public static ArrayList<Player> createPlayers(int numberOfPlayers) {
        if(numberOfPlayers <= 2 || numberOfPlayers > 6)
            throw new InvalidParameterException("minimum of 2 players, maximum of 6 players");

        ArrayList<Player> players = new ArrayList<Player>(numberOfPlayers);

        for (int i = 1; i <= numberOfPlayers; i++){
            Player player = new Player(i);
            players.add(player);
        }

        return players;
    }

    public static ArrayList<Player> getActivePlayers(ArrayList<Player> players){
        ArrayList<Player> activePlayers = new ArrayList<Player>();

        for(Player player: players){
            if (!player.isBusted()){
                activePlayers.add(player);
            }
        }

        return activePlayers;
    }
}
