package grpwk.game;

public enum Strategy {
    HIT("hit"),
    BLACK_JACK("win"),
    STICK("stick"),
    BUST("go bust");

    private final String name;

    Strategy(String name){
        this.name = name;
    }

    @Override
    public String toString() { return this.name; }
}
