package grpwk.game;

import java.util.ArrayList;

public class Game {
    private final Deck deck;
    private final ArrayList<Player> players;
    private Player winner = null;
    private boolean allStick = true;

    Game(int numberOfPlayers)
    {
        this.players = PlayerFactory.createPlayers(numberOfPlayers);
        this.deck = new Deck(); /* A deck of 52 cards */
    }

    public Game()
    {
        this(3);
    }

    private void dealCardTo(Player player){
        Card card = this.deck.pop();
        player.dealCard(card);
        System.out.println(player + " has been dealt " + card);
    }

    private void dealHands(){
        for (Player player: this.players){
            this.dealCardTo(player);
            this.dealCardTo(player);
        }
    }

    private void goBust(Player player){
        player.goBust();
        System.out.println(player + " busted!");
    }

    private void stick(Player player){
        System.out.println(player + " stick!");
    }

    private void handleStrategy(Strategy strategy, Player player){
        switch (strategy) {
            case HIT -> {
                this.dealCardTo(player);
                this.allStick = false;
            }
            case BUST -> {
                this.goBust(player);
                this.allStick = false;
            }
            case STICK -> this.stick(player);
            case BLACK_JACK -> {
                this.winner = player;
                this.allStick = false;
            }
            default -> throw new IllegalStateException("Unexpected value: " + strategy);
        }
    }

    private boolean shouldRun(){
        if (PlayerFactory.getActivePlayers(this.players).size() > 1){
            return winner == null && !this.allStick;
        }
        return false;
    }

    public void run() {
        this.deck.shuffle();
        this.dealHands();

        do {
            allStick = true;
            ArrayList<Player> activePlayers = PlayerFactory.getActivePlayers(this.players);
            for (Player player: activePlayers) {
                Strategy result = player.play();
                this.handleStrategy(result, player);
            }
        } while (this.shouldRun());

        if (this.winner == null){
            int highest = 0;
            for (Player player: PlayerFactory.getActivePlayers(this.players)){
                if (player.valueOfCards() > highest) {
                    winner = player;
                    highest = player.valueOfCards();
                }
            }
        }

        System.out.println(winner + " is the winner!");
    }
}
