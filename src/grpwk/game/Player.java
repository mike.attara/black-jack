package grpwk.game;

import grpwk.game.Card;
import grpwk.game.Deck;
import grpwk.game.Strategy;

import java.util.ArrayList;

public class Player {
    private ArrayList<Card> cards = new ArrayList<Card>();
    private String name;
    private boolean busted;

    public Player(int id)
    {
        this.name = "Player" + id;
    }

    public void dealCard(Card card)
    {
        this.cards.add(card);
    }

    public int valueOfCards(){
        return this.cards
                .stream()
                .map(Card::getValue)
                .reduce(0, (ac, v) -> ac + v );
    }

    private void hit(Deck deck){
        this.cards.add(deck.pop());
    }

    public Strategy play(){
        int total = this.valueOfCards();
        System.out.println("Total for " + this.name + " is " + total);
        if (total == 21) {
            return Strategy.BLACK_JACK;
        }
        else if (total > 21) {
            return Strategy.BUST;
        }
        else if (total >= 17) {
            return  Strategy.STICK;
        }
        return Strategy.HIT;
    }

    @Override
    public String toString() { return this.name; };

    public boolean isBusted(){
        return this.busted;
    }

    public void goBust(){
        this.busted = true;
    }
}
