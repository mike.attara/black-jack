package grpwk.game;

public class Card {
    private final Suit suit;
    private final Rank rank;

    public Card(Suit suit, Rank rank){
        this.suit = suit;
        this.rank = rank;
    }

    public int getValue(){
        return rank.rankValue;
    }

    @Override
    public String toString(){
        return this.rank + " of " + this.suit;
    }

}
