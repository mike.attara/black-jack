package grpwk.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
    private final ArrayList<Card> deck = new ArrayList<>();

    public Deck(){
        for (Suit suit: Suit.values()){
            for (Rank rank: Rank.values()){
                Card card = new Card(suit, rank);
                this.deck.add(card);
            }
        }
    }

    public void shuffle()
    {
        Random rand =  new Random();
        Collections.shuffle(deck, rand);
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (Card card: this.deck){
            output.append(card.toString()).append("\n");
        }
        return output.toString();
    }

    public Card pop() {
        int lastIndex = deck.size() - 1;
        Card card = deck.get(lastIndex);
        deck.remove(lastIndex);
        return card;
    }
}
