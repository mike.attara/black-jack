package grpwk;

import grpwk.game.Game;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to Blackjack");

        Game game = new Game();
        game.run();
    }
}