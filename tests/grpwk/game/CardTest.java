package grpwk.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CardTest {
    private Card card;

    @BeforeEach
    void setup(){
        card = new Card(Suit.CLUB, Rank.ACE);
    }

    @Test
    void testGetValue(){
        int expected = 11;
        int actual = card.getValue();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testCardName(){
        String expected = "Ace of Clubs";
        String actual = card.toString();
        Assertions.assertEquals(expected, actual);
    }
}
